№5. Циклічні конструкції
Мета: створення програми мовою С з домогою операторів for,while,do_while


1.	ВИМОГИ

1.1 Розробник

Інформація про розробника:
>	Демків С.С;
>	НТУ “ХПІ”, факультет “КІТ”, група 121-б.

1.2 Загальне завдання:

Створити елементарну програму для пошуку НОД двох чисел

2.Опис роботи

2.1 Створуємо основу програми, де визиваємо функції для кожного оператора та передаємо до функції значення чисел
 ```
int main()
 {
	 int a=21, b=56;
	 _for(a,b);
	 _while(a,b);
	 _do_while(a,b);
 }
```

2.2 За допомогою алгоритма Евкліда шукаємо у кожній функції НОД
2.2.1 Функція для оператора for:
```
void _for(int c,int d)//Функция поиска НОД с помощью for
        {
            for(int i=1; i>0;)
            {
                if(c!=0 && d!=0)
                {
                    if(c>d)
                    {
                        c=c%d;
                    }
                    else
                        d=d%c;
                }
                else
                break;
            }
            nod1=c+d;
        }
```

2.2.2 Функція для оператора while:
```
  void _while(int c,int d)//Функция поиска НОД с помощью while
        {
            while(1)
            {
                if(c!=0 && d!=0)
                {
                    if(c>d)
                    {
                        c=c%d;
                    }
                    else
                        d=d%c;
                }
                else
                break;	
                }	
            nod2=d+c;//Второй НОД
        }
```

2.2.3 Функція для оператора do_while	
```
void _while(int c,int d)//Функция поиска НОД с помощью while
        {
            while(1)
            {
                if(c!=0 && d!=0)
                {
                    if(c>d)
                    {
                        c=c%d;
                    }
                   else
                        d=d%c;
                }
                else
                break;	
                }	
            nod2=d+c;
        }	
```

Висновок
Була створена програма для пошуку за допомогою операторыв for, while, do_while НОД двух чисел 
