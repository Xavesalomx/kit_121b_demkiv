int size = 6;
int nod1, nod2, nod3; //Глобальные переменые для НОД для всех трех функций
void fibanachi(int *fib_number)
{
	for (int i = 2; i <= size; i++) {
		fib_number[i] = fib_number[i - 2] + fib_number[i - 1];
	}
}

void _for(int c, int d) //Функция поиска НОД с помощью for
{
	for (int i = 1; i > 0;) //Бесконечный цикл с проверкой и break
	{
		if (c != 0 && d != 0) // Алгоритм Эвклида
		{
			if (c > d) {
				c = c % d;
			} else
				d = d % c;
		} else
			break;
	}
	nod1 = c + d; // Первый НОД
}

void _while(int c, int d) //Функция поиска НОД с помощью while
{
	while (1) //Бесконечный цикл с проверкой
	{
		if (c != 0 && d != 0) //Алгоритм Эвклида
		{
			if (c > d) {
				c = c % d;
			} else
				d = d % c;
		} else
			break;
	}
	nod2 = d + c; //Второй НОД
}

void _do_while(int c, int d) //Функция поиска НОД с помощью do_while
{
	do {
		if (c != 0 && d != 0) //Алгоритм Эвклида
		{
			if (c > d) {
				c = c % d;
			} else
				d = d % c;
		} else
			break;
	} while (1); //Бесконечный цикл
	nod3 = d + c; //Третий НОД
}

int main()
{
	int a = 21, b = 56; //Переменые для которых ищем НОД
	int fib_number[size] = { 0, 1 };
	fibanachi(fib_number);
	_for(a, b); //Вызов функций
	_while(a, b);
	_do_while(a, b);
}
